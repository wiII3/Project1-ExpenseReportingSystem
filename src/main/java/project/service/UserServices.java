package project.service;

import project.model.ERS_USERS;
import project.dao.ERS_USERSDao;
import java.util.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class UserServices implements UserService {
	private ERS_USERSDao userDao;
	private static final Logger logger = LogManager.getLogger(UserServices.class);
	
	public UserServices(){
		this.userDao = new ERS_USERSDao();
	}
	
	public UserServices(ERS_USERSDao userDao) {
		this.userDao = userDao;
	}
	/*
	 * Verifies the login credentials exist.
	 * Returns true/false based on if exists.
	 */
	public Integer verifyLoginCredentials(String uname, String pass) {
		logger.info("Login of user:" +uname + " with pass:" + pass);
		ERS_USERS user = userDao.selectByUnamePass(uname, pass);
		if(user != null) {
			return user.getERS_USERS_ID();
		}else {
			return -1;
		}
	}
	
	public ERS_USERS retrieveUser(Integer userID){
		logger.info("Retrieving user" + userID);
		return userDao.selectERS_USERS(userID);
	}
	
	public List<ERS_USERS> retrieveAllUsers(){
		logger.info("Retrieving all user");
		return userDao.selectAll();
	}
	
	public Boolean registerUser(ERS_USERS user) {
		logger.info("Registering new user" + user);
		return userDao.insert(user);
	}

	public Boolean updateUser(ERS_USERS user) {
		logger.info("Updating user" + user);
		return userDao.update(user);
	}
	
	public Boolean deleteUser(Integer userID) {
		logger.info("Deleting user" + userID);
		
		ERS_USERS userToDelete = userDao.selectERS_USERS(userID);
		if(userToDelete==null) {
			return false;
		} else {
			return userDao.delete(userToDelete);
		}
	}

	public ERS_USERSDao getUserDao() {
		return userDao;
	}

	public void setUserDao(ERS_USERSDao userDao) {
		this.userDao = userDao;
	}
}
