package project.service;

import java.util.List;

import project.dao.ERS_REIMBURSEMENTDao;
import project.model.ERS_REIMBURSEMENT;
import project.model.REIMBURSEMENT_STATUS;
import project.model.REIMBURSEMENT_TYPE;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class ReimbursementServices implements ReimbursementService{
	
	private ERS_REIMBURSEMENTDao reiDao;
	private static final Logger logger = LogManager.getLogger(ReimbursementServices.class);
	
	public ReimbursementServices() {
		this.reiDao=new ERS_REIMBURSEMENTDao();
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementServices(ERS_REIMBURSEMENTDao reiDao) {
		this.reiDao = reiDao;
	}
	
	@Override
	public ERS_REIMBURSEMENT retrieveReimbursement(Integer id) {
		logger.info("Retrieving Reimbursement:" + id);
		return reiDao.selectERS_REIMBURSEMENT(id);
	}

	@Override
	public List<ERS_REIMBURSEMENT> retrieveAllReimbursements() {
		logger.info("Retrieving ALL reimbursements");
		return reiDao.selectAll();
	}

	@Override
	public List<ERS_REIMBURSEMENT> retrieveAllReimbursementsByType(REIMBURSEMENT_TYPE type) {
		logger.info("Retrieving ALL reimbursements by type:" +type);
		return reiDao.selectByREIMB_TYPE(type);
	}

	@Override
	public List<ERS_REIMBURSEMENT> retrieveAllReimbursementsByStatus(REIMBURSEMENT_STATUS status) {
		logger.info("Retrieving ALL reimbursements by status:" +status);
		//System.out.println("RETRIEVE BY STATUS START:" + status);
		return reiDao.selectByStatusQuery(status);	
	}

	@Override
	public List<ERS_REIMBURSEMENT> retrieveAllReimbursementsByStatusAndUserID(REIMBURSEMENT_STATUS status, Integer id) {
		logger.info("Retrieving ALL reimbursements by status:" + status +"and user id:");
		System.out.println("RETRIEVE BY STATUS AND ID START:" + status + "ID:" + id);
		return reiDao.selectByStatusAndIDQuery(status, id);	
	}
	
	@Override
	public List<ERS_REIMBURSEMENT> retrieveAllReimbursementsByAuthorID(Integer AuthorID) {
		logger.info("Retrieving ALL reimbursements by authorID:" +AuthorID);
		return reiDao.selectByAuthorID(AuthorID);
	}

	@Override
	public Boolean createReimbursement(ERS_REIMBURSEMENT bursement) {
		logger.info("Create Reimbursement:"+bursement);
		return reiDao.insert(bursement);
	}

	@Override
	public Boolean updateReimbursement(ERS_REIMBURSEMENT bursement) {
		logger.info("Update Reimbursement:"+bursement);
		return reiDao.update(bursement);
	}

	@Override
	public Boolean deleteReimbursement(ERS_REIMBURSEMENT bursement) {
		logger.info("Delete Reimbursement:"+bursement);
		return reiDao.delete(bursement);
	}

	public ERS_REIMBURSEMENTDao getEriDao() {
		return reiDao;
	}

	public void setEriDao(ERS_REIMBURSEMENTDao reiDao) {
		this.reiDao = reiDao;
	}

	
}
