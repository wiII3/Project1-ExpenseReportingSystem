package project;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.google.gson.Gson;

import project.model.*;
import project.service.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.web.WebLoggerContextUtils;

/**
 * Servlet implementation class GetUnapproved
 */
public class GetUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserServices uService = new UserServices();
	public void init() {
    }
    private void logIn(Map<String, String> inputMap, HttpServletRequest request, PrintWriter out) {
    	
    	if(inputMap.get("username")!=null && inputMap.get("password")!=null){
			String username = (String)inputMap.get("username");
			String password = (String)inputMap.get("password");
			Integer userID = uService.verifyLoginCredentials(username, password);
			if(userID>0) {
				ERS_USERS use = uService.retrieveUser(userID);
				request.getSession().setAttribute("username", username); 
				request.getSession().setAttribute("user", use); 
				request.getSession().setAttribute("userID", use.getERS_USERS_ID()); 
				request.getSession().setAttribute("role", use.getUSER_ROLE());
				String toReturn = "{\"message\":\"Success\""+
						",\"role\":\""+ use.getUSER_ROLE() + "\""+
						",\"uname\":\""+ username + "\""+
						",\"pass\":\""+ password + "\""+
						",\"email\":\""+ use.getUSER_EMAIL() + "\""+
						",\"fname\":\""+ use.getUSER_FIRST_NAME() + "\""+
						",\"lname\":\""+ use.getUSER_LAST_NAME() + "\""+
						",\"userID\":\""+ use.getERS_USERS_ID() + "\""+
						",\"nextPage\":\"home\"}";
				System.out.println(toReturn);
				System.out.println("NewUserID:"+request.getSession().getAttribute("userID"));
				out.append(toReturn);
			}
		}
    }

    private void logOut(HttpServletResponse response) throws IOException {
    	response.sendRedirect("LogOut");
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @CrossOrigin(origins = "http://localhost:3000")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("START GET Resolved");
		doPost(request, response);
	}

    @CrossOrigin(origins = "http://localhost:3000")
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	final Logger logger = LogManager.getLogger(GetUsers.class);
        logger.info("Hello, servlet!"); 
        //List of allowed origins
//        List<String> incomingURLs = Arrays.asList(getServletContext().getInitParameter("incomingURLs").trim().split(","));
//        // Get client's origin
//        String clientOrigin = request.getHeader("origin");
//        // Get client's IP address
//        String ipAddress = request.getHeader("x-forwarded-for");
//        if (ipAddress == null) {
//            ipAddress = request.getRemoteAddr();
//        }
        
        
		String theInput = request.getReader().readLine();
		PrintWriter out = response.getWriter();
		response.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		System.out.println("INCOMING:" + theInput);
		Gson gson = new Gson();
		Map<String,String> inputMap = new HashMap<String,String>();
		try {
		inputMap = gson.fromJson(theInput, Map.class);
		} catch(Exception e) {
			System.out.println("GSON ISSUE:" +e);
			String[] aList = theInput.split("&");
			for(int i = 0; i < aList.length; i++) {
				System.out.println("aLISTING :" +aList[i]);
				String[] tempList = aList[i].split("=");
				for(int j = 0; j < tempList.length;j++) {
					System.out.println("tempList :"+j +":"+ tempList[j]);
				}
				inputMap.put((String)tempList[0], (String)tempList[1]);
			}
		}
		Boolean toLogin = false;
		try {
			for(String key:inputMap.keySet()) {
				System.out.println("KEY:"+key + ":VAL:"+inputMap.get(key));
			}
			if(((String)inputMap.get("name")).equals("Login")) {
				toLogin=true;
				System.out.println("TOLOGIN TRUE?" + toLogin);
			}
			System.out.println("DID?"+inputMap.get("name"));
		} catch(Exception e) {
			System.out.println(e);
		}
		System.out.println("CHECKING USER ID:" + request.getSession().getAttribute("userID"));
		if(request.getSession().getAttribute("userID")==null||toLogin) {
			logIn(inputMap, request, out);
		} else {
			Integer userID=null; String name=null; Integer choice=null; Integer version=null;
	
			try { 
				userID = (Integer) request.getSession().getAttribute("userID");
			} catch(Exception e) {
				System.out.println("NO USER ID!");
				System.out.println(e);
			}
			System.out.println("START POST USERS");
			try {
				name = request.getParameter("name");
			} catch(Exception e) {
				System.out.println("NAME EXCEPTION"+e);
			}
			System.out.println("NAME NAME NAME");
			try {
				if(name == null) {
					if(inputMap.get("name")!= null) {
						name = (String) inputMap.get("name");
					}
				}
			} catch(Exception e) {
				System.out.println("EXCEPT NAME AGAIN" + e);
			}
			System.out.println("NAME:"+name);
			try {
			version = Integer.parseInt(request.getParameter("version"));
			} catch(Exception e) {
				System.out.println("NO VERSION!");
				System.out.println(e);
				if(inputMap.get("version")!=null) {
					System.out.println("VersionInException:"+inputMap.get("version"));
					try {
					version = Integer.decode((String) inputMap.get("version"));
					} catch(Exception e1) {
						System.out.println(e1);
						version = Integer.parseInt(inputMap.get("version"));
					}
				}
			}
			try {
			choice = Integer.parseInt(request.getParameter("choice"));
			} catch(Exception e) {
				System.out.println("NO CHOICE!");
				System.out.println(e);
				if(inputMap.get("choice")!=null) {
					System.out.println("ChoiceInException:"+inputMap.get("choice"));
					try {
					choice = Integer.decode((String) inputMap.get("choice"));
					} catch(Exception e1) {
						System.out.println("NO CHOICE!2");
						System.out.println(e1);
						try {
							choice = Integer.decode((String) inputMap.get("choice"));
						} catch(Exception e2) {
							System.out.println(e2);
							choice = Integer.valueOf(inputMap.get("choice"));
						}
					}
				}
			}
			new ArrayList<>();
	
			if(name.equals("Get") && choice != null && userID != null) {
				System.out.println("USER GET ONE");
				if(request.getSession().getAttribute("role").toString().equals("EMPLOYEE")) {
					/*
					 * Employee requesting personal profile data.
					 */
					System.out.println("EMPLOYEEE?");
					if(choice.equals(0)) {
						ERS_USERS user = uService.retrieveUser(userID);//uDao.selectERS_USERS(userID);
						Map<String, String> myMap = new HashMap<>();
						myMap.put("USERNAME", user.getERS_USERNAME());
						myMap.put("PASSWORD", user.getERS_PASSWORD());
						myMap.put("EMAIL", user.getUSER_EMAIL());
						myMap.put("FNAME", user.getUSER_FIRST_NAME());
						myMap.put("LNAME", user.getUSER_LAST_NAME());
						
						String theData = gson.toJson(myMap); 
						System.out.println(theData);
						out.append(theData);
					}/*
					 * Employee requesting someone else's profile data.
					 */
					else {
						
						logOut(response);
					}
				/*
				 * MANAGER Requesting an employees profile data. 
				 */
				}else if(request.getSession().getAttribute("role").equals("MANAGER")) {
					System.out.println("MANAGER");
				
				}
				/*
				 * Others requesting users profile data.
				 */
				else {
					System.out.println("NEITHER EMPLOYEE OR MANAGER");
					System.out.println(request.getSession().getAttribute("role"));
					logOut(response);
				}
			}
			
			/*
			 * GETTING ALL OF THE USERS. To list. get
			 */
			else if(name.equals("GetAll") && choice != null && userID != null) {
				System.out.println("USER GET ALL CHOICE:" + choice);
				
				List<ERS_USERS> tempUsers = uService.retrieveAllUsers();
				List<ERS_USERS> toSendUsers = new ArrayList<>();
				for(ERS_USERS aUse : tempUsers) {
					if(aUse.getUSER_ROLE().equals(USER_ROLES.EMPLOYEE)) {
						toSendUsers.add(aUse);
					}
				}
				String theData = gson.toJson(toSendUsers);
				List<String> storage = new ArrayList<>();
				storage.add(12345+"");
				storage.add(theData);
				String toSend = gson.toJson(storage);
				System.out.println(toSend);
				out.append(toSend);
			}
			
			/*
			 * UPDATING THE CURRENT USER. //put post?
			 */
			else if(name.equals("Update") && choice != null && userID != null) {
				System.out.println("USER UPDATE");
				ERS_USERS user = uService.retrieveUser(userID);//uDao.selectERS_USERS(userID);
				
				user.setERS_USERNAME(request.getParameter("Username"));
				if(user.getERS_USERNAME()==null) {
					user.setERS_USERNAME(inputMap.get("Username"));	
				}
				user.setERS_PASSWORD(request.getParameter("Password"));
				if(user.getERS_PASSWORD()==null) {
					user.setERS_PASSWORD(inputMap.get("Password"));	
				}
				user.setUSER_EMAIL(request.getParameter("Email"));
				if(user.getUSER_EMAIL()==null) {
					user.setUSER_EMAIL(inputMap.get("Email"));	
				}
				user.setUSER_FIRST_NAME(request.getParameter("FirstName"));
				if(user.getUSER_FIRST_NAME()==null) {
					user.setUSER_FIRST_NAME(inputMap.get("FirstName"));	
				}
				user.setUSER_LAST_NAME(request.getParameter("LastName"));
				if(user.getUSER_LAST_NAME()==null) {
					user.setUSER_LAST_NAME(inputMap.get("LastName"));	
				}
				uService.updateUser(user);
				out.append("{\"message\":\"User Updated\"}");
			}
			
			/*
			 * CREATING A NEW USER! //put?
			 */
			else if(name.equals("Insert") && choice != null && userID != null) {
				System.out.println("USER INSERT");
				ERS_USERS user = new ERS_USERS();
				user.setERS_USERNAME(request.getParameter("Username"));	
				user.setERS_PASSWORD(request.getParameter("Password"));
				user.setUSER_EMAIL(request.getParameter("Email"));
				user.setUSER_FIRST_NAME(request.getParameter("FirstName"));
				user.setUSER_LAST_NAME(request.getParameter("LastName"));
				uService.registerUser(user);
			}
		}
		
		
//		if(name.equals("Approve") && choice != null && userID != null) {
//			ERS_USERS use = (ERS_USERS) request.getSession().getAttribute("user");
//			ERS_REIMBURSEMENT toUpdate = eDao.selectERS_REIMBURSEMENT(choice);
//			toUpdate.setREIMB_RESLOVER(use);
//			toUpdate.setREIMB_STATUS(REIMBURSEMENT_STATUS.APPROVED);
//			eDao.update(toUpdate);
//			System.out.println("IN POST APPROVE");
//			out.append("{\"NUMBER\":5,\"MESSAGE\":\"APPROVED\", \"TARGET\":\""+choice+"\"}");
//		} else if(name.equals("Deny") && choice != null && userID != null){
//			ERS_REIMBURSEMENT toUpdate = eDao.selectERS_REIMBURSEMENT(choice);
//			ERS_USERSDao uDao = new ERS_USERSDao();
//			ERS_USERS use = (ERS_USERS) uDao.selectERS_USERS(userID);
//			System.out.println("USERID"+userID);
//			System.out.println("USER"+use.getERS_USERS_ID());
//			toUpdate.setREIMB_RESLOVER(use);
//			toUpdate.setREIMB_STATUS(REIMBURSEMENT_STATUS.DENIED);
//			eDao.update(toUpdate);
//			System.out.println("IN POST DENY");
//			out.append("{\"NUMBER\":5,\"MESSAGE\":\"DENIED\", \"TARGET\":\""+choice+"\"}");
//		} else if(name.equals("Pending") && version != null) {
//			Query<ERS_REIMBURSEMENT> query = ses.getNamedQuery("findReimbursmentsByStatusID")
//					.setParameter("REIMB_STATUS", REIMBURSEMENT_STATUS.PENDING);
//			List<ERS_REIMBURSEMENT> reimList = query.list();
//			//List<ERS_REIMBURSEMENT> reimList = eDao.selectByREIMB_STATUS(REIMBURSEMENT_STATUS.PENDING);
//			theLists.add(reimList);
//			System.out.println("IN POST GET-PENDING");
//		} else if(name.equals("Resolved") && version != null) {
//			Query<ERS_REIMBURSEMENT> query = ses.getNamedQuery("findReimbursmentsByStatusID")
//					.setParameter("REIMB_STATUS", REIMBURSEMENT_STATUS.APPROVED);
//			List<ERS_REIMBURSEMENT> reimLista = query.list();
//			//List<ERS_REIMBURSEMENT> reimLista = eDao.selectByREIMB_STATUS(REIMBURSEMENT_STATUS.APPROVED);
//			Query<ERS_REIMBURSEMENT> query1 = ses.getNamedQuery("findReimbursmentsByStatusID")
//					.setParameter("REIMB_STATUS", REIMBURSEMENT_STATUS.DENIED);
//			List<ERS_REIMBURSEMENT> reimListd = query1.list();
//			//List<ERS_REIMBURSEMENT> reimListd = eDao.selectByREIMB_STATUS(REIMBURSEMENT_STATUS.DENIED);
//			theLists.add(reimLista);
//			theLists.add(reimListd);
//			System.out.println("IN POST GET-RESOLVED");
//		}
//		List<Map<String,String>> easySend = new ArrayList<>();
//		
//		try {
//			for(List<ERS_REIMBURSEMENT> reimList: theLists) {
//				for(ERS_REIMBURSEMENT rei:reimList) {
//					Map<String,String> toAdd = new HashMap<>();
//														   toAdd.put("ID"			,rei.getREIMB_ID()+"");
//														   toAdd.put("AMOUNT"		,rei.getREIMB_AMOUNT()+"");
//														   toAdd.put("RECEIPT"		,rei.getREIMB_RECEIPT()+"");
//					if(rei.getREIMB_DESCRIPTION()!=null) { toAdd.put("DESCRIPTION"	,rei.getREIMB_DESCRIPTION()+" "); }
//					else { 								   toAdd.put("DESCRIPTION"	,"null"); }
//														   toAdd.put("AUTHOR"		,rei.getREIMB_AUTHOR().getERS_USERS_ID()+"");
//					if(rei.getREIMB_RESLOVER()!=null) {    toAdd.put("RESLOVER"		,rei.getREIMB_RESLOVER().getERS_USERS_ID()+""); }
//					else { 								   toAdd.put("RESLOVER"		,"null");}
//					if(rei.getREIMB_RESOLVED()!=null) {    toAdd.put("RESOLVED"		,rei.getREIMB_RESOLVED()+"");
//					}else { 							   toAdd.put("RESOLVED"		,"null"); }
//														   toAdd.put("STATUS"		,rei.getREIMB_STATUS()+"");
//														   toAdd.put("SUBMITTED"	,rei.getREIMB_SUBMITTED()+"");
//														   toAdd.put("TYPE"			,rei.getREIMB_TYPE()+"");
//					easySend.add(toAdd);
//				}
//			}
//			String theData = "";
//			if(easySend.size()>0) 
//			{ 
//				theData = mapper.writeValueAsString(easySend); 
//				List<String> storage = new ArrayList<>();
//				storage.add(12345+"");
//				storage.add(theData);
//				String toSend = mapper.writeValueAsString(storage);
//				System.out.println("RETURN POST REIMBURSMENT INSIDE");
//				System.out.println(toSend);
//				out.append(toSend);
//			} else { System.out.println("RETURN POST REIMBURSMENT OUTSIDE"); }
//		}catch (Exception e){
//			System.out.println(e);
//			System.out.println("###########################################");
//			System.out.println("########### REIMBURSMENT BROKE! ###########");
//			System.out.println("########### REIMBURSMENT BROKE! ###########");
//			System.out.println("########### REIMBURSMENT BROKE! ###########");
//			System.out.println("###########################################");
//			out.append(e.toString());
//		}
	}
}
