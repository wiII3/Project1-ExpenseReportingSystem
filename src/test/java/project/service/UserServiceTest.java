package project.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import project.dao.ERS_USERSDao;
import project.model.ERS_USERS;

public class UserServiceTest {
	
	private ERS_USERSDao userDaoMock;
	private UserServices uService;
	
	@Before
	public void setup() {
		userDaoMock = mock(ERS_USERSDao.class);
		uService = new UserServices(userDaoMock);
	}
	
	@Test
	public void defaultsTesting() {
		userDaoMock = mock(ERS_USERSDao.class);
		uService = new UserServices();
		uService.setUserDao(userDaoMock);
		ERS_USERSDao aDao = uService.getUserDao();
		assertTrue(aDao.equals(userDaoMock));
	}
	
	/*
	 * Testing the retrieveUser Method:
	 */
	@Test
	public void retrieveUserTrue() {
		ERS_USERS user1 = new ERS_USERS();
		user1.setERS_USERS_ID(1);
		when(userDaoMock.selectERS_USERS(1)).thenReturn(user1);
		assertTrue(uService.retrieveUser(1).getERS_USERS_ID().equals(user1.getERS_USERS_ID()));
	}
	
	@Test
	public void retrieveUserFalse() {
		ERS_USERS user1 = new ERS_USERS();
		user1.setERS_USERS_ID(1);
		ERS_USERS user2 = new ERS_USERS();
		user2.setERS_USERS_ID(2);
		when(userDaoMock.selectERS_USERS(1)).thenReturn(user1);
		assertFalse(uService.retrieveUser(1).getERS_USERS_ID().equals(user2.getERS_USERS_ID()));
	}
	
	@Test
	public void retrieveUserNull() {
		ERS_USERS user1 = null;
		when(userDaoMock.selectERS_USERS(1)).thenReturn(user1);
		assertNull(uService.retrieveUser(1));
	}
	
	/*
	 * Testing the verifyLoginCredentials Method:
	 */
	@Test
	public void verifyLoginCredentialsTrue() {
		String uname = "bob";
		String pass = "pass";
		
		ERS_USERS user1 = new ERS_USERS();
		user1.setERS_USERNAME(uname);
		user1.setERS_PASSWORD(pass);
		user1.setERS_USERS_ID(5);
		when(userDaoMock.selectByUnamePass(uname, pass)).thenReturn(user1);
		assertTrue(uService.verifyLoginCredentials(uname, pass)>=0);
	}
	
	@Test
	public void verifyLoginCredentialsFalse() {
		String uname = "bob";
		String pass = "pass";
		ERS_USERS user1 = null;
		when(userDaoMock.selectByUnamePass(uname, pass)).thenReturn(user1);
		assertFalse(uService.verifyLoginCredentials(uname, pass)>0);
	}
	
	/*
	 * Testing the retrieveAllUsers Method:
	 */
	@Test
	public void retrieveAllUsersTrue() {
		ERS_USERS user1 = new ERS_USERS();
		List<ERS_USERS> users = Arrays.asList(user1);
		when(userDaoMock.selectAll()).thenReturn(users);
		assertTrue(uService.retrieveAllUsers().equals(users));
	}
	
	@Test
	public void retrieveAllUsersFalse() {
		ERS_USERS user1 = new ERS_USERS();
		ERS_USERS user2 = new ERS_USERS();
		List<ERS_USERS> users = Arrays.asList(user1);
		List<ERS_USERS> users2 = Arrays.asList(user2);
		when(userDaoMock.selectAll()).thenReturn(users);
		assertFalse(uService.retrieveAllUsers().equals(users2));
	}
	
	@Test
	public void retrieveAllUsersNull() {
		List<ERS_USERS> users = null;
		when(userDaoMock.selectAll()).thenReturn(users);
		assertNull(uService.retrieveAllUsers());
	}
	
	/*
	 * Testing the registerUser Method:
	 */
	@Test
	public void registerUserTrue() {
		ERS_USERS user1 = new ERS_USERS();
		when(userDaoMock.insert(user1)).thenReturn(true);
		assertTrue(uService.registerUser(user1).equals(true));
	}
	
	@Test
	public void registerUserFalse() {
		ERS_USERS user1 = new ERS_USERS();
		when(userDaoMock.insert(user1)).thenReturn(false);
		assertTrue(uService.registerUser(user1).equals(false));
	}
	
	@Test
	public void registerUserNull() {
		ERS_USERS user1 = new ERS_USERS();
		when(userDaoMock.insert(user1)).thenReturn(null);
		assertNull(uService.registerUser(user1));
	}

	/*
	 * Testing the updateUser Method:
	 */
	@Test
	public void updateUserTrue() {
		ERS_USERS user1 = new ERS_USERS();
		when(userDaoMock.update(user1)).thenReturn(true);
		assertTrue(uService.updateUser(user1).equals(true));
	}
	
	@Test
	public void updateUserFalse() {
		ERS_USERS user1 = new ERS_USERS();
		when(userDaoMock.update(user1)).thenReturn(false);
		assertTrue(uService.updateUser(user1).equals(false));
	}
	
	@Test
	public void updateUserNull() {
		ERS_USERS user1 = new ERS_USERS();
		when(userDaoMock.update(user1)).thenReturn(null);
		assertNull(uService.updateUser(user1));
	}
	
	/*
	 * Testing the deleteUser Method:
	 */
	@Test
	public void deleteUserTrue() {
		Integer userID = 4;
		ERS_USERS user1 = new ERS_USERS();
		when(userDaoMock.delete(user1)).thenReturn(true);
		when(userDaoMock.selectERS_USERS(userID)).thenReturn(user1);
		assertTrue(uService.deleteUser(userID));
	}
	
	@Test
	public void deleteUserFalse() {
		Integer userID = 4;
		ERS_USERS user1 = new ERS_USERS();
		user1.setERS_USERS_ID(userID);
		when(userDaoMock.delete(user1)).thenReturn(false);
		when(userDaoMock.selectERS_USERS(userID)).thenReturn(null);
		assertFalse(uService.deleteUser(userID));
	}
	
	@Test
	public void deleteUserNull() {
		Integer userID = 4;
		when(userDaoMock.selectERS_USERS(userID)).thenReturn(null);
		assertFalse(uService.deleteUser(userID));
	}
	
	
	
	
	
	
}
