package project.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import project.dao.ERS_REIMBURSEMENTDao;
import project.model.ERS_REIMBURSEMENT;
import project.model.ERS_USERS;
import project.model.REIMBURSEMENT_STATUS;
import project.model.REIMBURSEMENT_TYPE;

public class ReimbursementServiceTest {
	private ERS_REIMBURSEMENTDao bursementMock;
	private ReimbursementServices rService;
	
	@Before
	public void setup() {
		bursementMock = mock(ERS_REIMBURSEMENTDao.class);
		rService = new ReimbursementServices(bursementMock);
	}
	
	@Test
	public void defaultsTesting() {
		bursementMock = mock(ERS_REIMBURSEMENTDao.class);
		rService = new ReimbursementServices();
		rService.setEriDao(bursementMock);
		ERS_REIMBURSEMENTDao aDao = rService.getEriDao();
		assertTrue(aDao.equals(bursementMock));
	}
	

	/*
	 * Testing the retrieveReimbursement Method:
	 */
	@Test
	public void retrieveReimbursementTrue() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		when(bursementMock.selectERS_REIMBURSEMENT(1)).thenReturn(ers1);
		assertTrue((Integer.valueOf(rService.retrieveReimbursement(1).getREIMB_ID())).equals(Integer.valueOf((ers1.getREIMB_ID()))));
	}
	
	@Test
	public void retrieveReimbursementFalse() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		ERS_REIMBURSEMENT ers2 = new ERS_REIMBURSEMENT();
		ers2.setREIMB_ID(2);
		when(bursementMock.selectERS_REIMBURSEMENT(1)).thenReturn(ers1);
		assertFalse((Integer.valueOf(rService.retrieveReimbursement(1).getREIMB_ID())).equals(Integer.valueOf((ers2.getREIMB_ID()))));
	}
	
	@Test
	public void retrieveReimbursementNull() {
		ERS_REIMBURSEMENT ers1 = null;
		when(bursementMock.selectERS_REIMBURSEMENT(1)).thenReturn(ers1);
		assertNull(rService.retrieveReimbursement(1));
	}

	/*
	 * Testing the retrieveAllReimbursements Method:
	 */
	@Test
	public void retrieveAllReimbursementsTrue() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList = new ArrayList<>();
		aList.add(ers1);
		when(bursementMock.selectAll()).thenReturn(aList);
		assertTrue(rService.retrieveAllReimbursements().equals(aList));
	}
	
	@Test
	public void retrieveAllReimbursementsFalse() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList = new ArrayList<>();
		aList.add(ers1);
		ERS_REIMBURSEMENT ers2 = new ERS_REIMBURSEMENT();
		ers2.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList2 = new ArrayList<>();
		aList2.add(ers2);
		when(bursementMock.selectAll()).thenReturn(aList);
		assertFalse(rService.retrieveAllReimbursements().equals(aList2));
	}
	
	@Test
	public void retrieveAllReimbursementsNull() {
		List<ERS_REIMBURSEMENT> aList = null;
		when(bursementMock.selectAll()).thenReturn(aList);
		assertNull(rService.retrieveAllReimbursements());
	}

	/*
	 * Testing the retrieveAllReimbursementsByType Method:
	 */
	@Test
	public void retrieveAllReimbursementsByTypeTrue() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList = new ArrayList<>();
		aList.add(ers1);
		when(bursementMock.selectByREIMB_TYPE(REIMBURSEMENT_TYPE.FOOD)).thenReturn(aList);
		assertTrue(rService.retrieveAllReimbursementsByType(REIMBURSEMENT_TYPE.FOOD).equals(aList));
	}
	
	@Test
	public void retrieveAllReimbursementsByTypeFalse() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList = new ArrayList<>();
		aList.add(ers1);
		ERS_REIMBURSEMENT ers2 = new ERS_REIMBURSEMENT();
		ers2.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList2 = new ArrayList<>();
		aList2.add(ers2);
		when(bursementMock.selectByREIMB_TYPE(REIMBURSEMENT_TYPE.FOOD)).thenReturn(aList);
		assertFalse(rService.retrieveAllReimbursementsByType(REIMBURSEMENT_TYPE.FOOD).equals(aList2));
	}
	
	@Test
	public void retrieveAllReimbursementsByTypeNull() {
		List<ERS_REIMBURSEMENT> aList = null;
		when(bursementMock.selectByREIMB_TYPE(REIMBURSEMENT_TYPE.FOOD)).thenReturn(aList);
		assertNull(rService.retrieveAllReimbursementsByType(REIMBURSEMENT_TYPE.FOOD));
	}

	/*
	 * Testing the retrieveAllReimbursementsByStatus Method:
	 */
	@Test
	public void retrieveAllReimbursementsByStatusTrue() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList = new ArrayList<>();
		aList.add(ers1);
		when(bursementMock.selectByStatusQuery(REIMBURSEMENT_STATUS.APPROVED)).thenReturn(aList);
		assertTrue(rService.retrieveAllReimbursementsByStatus(REIMBURSEMENT_STATUS.APPROVED).equals(aList));
	}
	
	@Test
	public void retrieveAllReimbursementsByStatusFalse() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList = new ArrayList<>();
		aList.add(ers1);
		ERS_REIMBURSEMENT ers2 = new ERS_REIMBURSEMENT();
		ers2.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList2 = new ArrayList<>();
		aList2.add(ers2);
		when(bursementMock.selectByStatusQuery(REIMBURSEMENT_STATUS.APPROVED)).thenReturn(aList);
		assertFalse(rService.retrieveAllReimbursementsByStatus(REIMBURSEMENT_STATUS.APPROVED).equals(aList2));
	}
	
	@Test
	public void retrieveAllReimbursementsByStatusNull() {
		List<ERS_REIMBURSEMENT> aList = null;
		when(bursementMock.selectByStatusQuery(REIMBURSEMENT_STATUS.APPROVED)).thenReturn(aList);
		assertNull(rService.retrieveAllReimbursementsByStatus(REIMBURSEMENT_STATUS.APPROVED));
	}

	/*
	 * Testing the retrieveAllReimbursementsByStatusAndUserID Method:
	 */
	@Test
	public void retrieveAllReimbursementsByStatusAndUserIDTrue() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		ERS_USERS user1 = new ERS_USERS();
		user1.setERS_USERS_ID(12345);
		List<ERS_REIMBURSEMENT> aList = new ArrayList<>();
		aList.add(ers1);
		when(bursementMock.selectByStatusAndIDQuery(REIMBURSEMENT_STATUS.APPROVED, user1.getERS_USERS_ID())).thenReturn(aList);
		assertTrue(rService.retrieveAllReimbursementsByStatusAndUserID(REIMBURSEMENT_STATUS.APPROVED,user1.getERS_USERS_ID()).equals(aList));
	}
	
	@Test
	public void retrieveAllReimbursementsByStatusAndUserIDFalse() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		ERS_USERS user1 = new ERS_USERS();
		user1.setERS_USERS_ID(12345);
		List<ERS_REIMBURSEMENT> aList = new ArrayList<>();
		aList.add(ers1);
		ERS_REIMBURSEMENT ers2 = new ERS_REIMBURSEMENT();
		ers2.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList2 = new ArrayList<>();
		aList2.add(ers2);
		when(bursementMock.selectByStatusAndIDQuery(REIMBURSEMENT_STATUS.APPROVED, user1.getERS_USERS_ID())).thenReturn(aList);
		assertFalse(rService.retrieveAllReimbursementsByStatusAndUserID(REIMBURSEMENT_STATUS.APPROVED,user1.getERS_USERS_ID()).equals(aList2));
	}
	
	@Test
	public void retrieveAllReimbursementsByStatusAndUserIDNull() {
		List<ERS_REIMBURSEMENT> aList = null;
		ERS_USERS user1 = new ERS_USERS();
		user1.setERS_USERS_ID(12345);
		when(bursementMock.selectByStatusAndIDQuery(REIMBURSEMENT_STATUS.APPROVED, user1.getERS_USERS_ID())).thenReturn(aList);
		assertNull(rService.retrieveAllReimbursementsByStatusAndUserID(REIMBURSEMENT_STATUS.APPROVED,user1.getERS_USERS_ID()));
	}

	/*
	 * Testing the retrieveAllReimbursementsByAuthorID Method:
	 */
	@Test
	public void retrieveAllReimbursementsByAuthorIDTrue() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		ERS_USERS user1 = new ERS_USERS();
		user1.setERS_USERS_ID(12345);
		List<ERS_REIMBURSEMENT> aList = new ArrayList<>();
		aList.add(ers1);
		when(bursementMock.selectByAuthorID(user1.getERS_USERS_ID())).thenReturn(aList);
		assertTrue(rService.retrieveAllReimbursementsByAuthorID(user1.getERS_USERS_ID()).equals(aList));
	}
	
	@Test
	public void retrieveAllReimbursementsByAuthorIDFalse() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		ERS_USERS user1 = new ERS_USERS();
		user1.setERS_USERS_ID(12345);
		List<ERS_REIMBURSEMENT> aList = new ArrayList<>();
		aList.add(ers1);
		ERS_REIMBURSEMENT ers2 = new ERS_REIMBURSEMENT();
		ers2.setREIMB_ID(1);
		List<ERS_REIMBURSEMENT> aList2 = new ArrayList<>();
		aList2.add(ers2);
		when(bursementMock.selectByAuthorID(user1.getERS_USERS_ID())).thenReturn(aList);
		assertFalse(rService.retrieveAllReimbursementsByAuthorID(user1.getERS_USERS_ID()).equals(aList2));
	}
	
	@Test
	public void retrieveAllReimbursementsByAuthorIDNull() {
		List<ERS_REIMBURSEMENT> aList = null;
		ERS_USERS user1 = new ERS_USERS();
		user1.setERS_USERS_ID(12345);
		when(bursementMock.selectByAuthorID(user1.getERS_USERS_ID())).thenReturn(aList);
		assertNull(rService.retrieveAllReimbursementsByAuthorID(user1.getERS_USERS_ID()));
	}
	

	/*
	 * Testing the createReimbursement Method:
	 */
	@Test
	public void createReimbursementTrue() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		when(bursementMock.insert(ers1)).thenReturn(true);
		assertTrue(rService.createReimbursement(ers1));
	}
	
	@Test
	public void createReimbursementFalse() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		when(bursementMock.insert(ers1)).thenReturn(false);
		assertFalse(rService.createReimbursement(ers1));
	}
	
	/*
	 * Testing the updateReimbursement Method:
	 */
	@Test
	public void updateReimbursementTrue() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		when(bursementMock.update(ers1)).thenReturn(true);
		assertTrue(rService.updateReimbursement(ers1));
	}
	
	@Test
	public void updateReimbursementFalse() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		when(bursementMock.update(ers1)).thenReturn(false);
		assertFalse(rService.updateReimbursement(ers1));
	}
	
	/*
	 * Testing the deleteReimbursement Method:
	 */
	@Test
	public void deleteReimbursementTrue() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		when(bursementMock.delete(ers1)).thenReturn(true);
		assertTrue(rService.deleteReimbursement(ers1));
	}
	
	@Test
	public void deleteReimbursementFalse() {
		ERS_REIMBURSEMENT ers1 = new ERS_REIMBURSEMENT();
		ers1.setREIMB_ID(1);
		when(bursementMock.delete(ers1)).thenReturn(false);
		assertFalse(rService.deleteReimbursement(ers1));
	}
}
