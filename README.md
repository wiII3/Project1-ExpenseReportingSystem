# MyERS
# Project 1: Expense Reimbursement System

## Executive Summary

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

## Technical Requirements

- [X]   The application employs the DAO design pattern, and properly separate your code into the appropriate layers
- [X]   The back-end system uses Hibernate to map Java objects to a MySQL database
- [X]   The application is deployed onto a Tomcat Server
- [X]   The middle tier uses Servlet technology for dynamic Web application development
- [X]   The front-end view can use JavaScript and use AJAX to call server-side components.
- [X]   The web pages should look presentable.
- [X]   The service methods implement Log4J and JUnit, with over 75% code coverage.
- [X]   Users can upload a document or image of their receipt when submitting reimbursements

## User Stories

As an employee I can:

- [X]   Login
- [X]   Logout
- [X]   View the employee home page
- [X]   Submit a reimbursement request
- [X]   View pending reimbursement requests
- [X]   View resolved reimbursement requests
- [X]   View my account information
- [X]   Update account information

As a manager I can:

- [X]   Login
- [X]   Logout
- [X]   View the manager home page
- [X]   Approve/Deny pending reimbursement requests
- [X]   View all pending requests of all employees
- [X]   View all resolved requests of all employees
- [X]   View reimbursement requests of a specific employee
- [X]   View all employees

## Reimbursement Types

- [X]   Employees must select the type of reimbursement as: LODGING, TRAVEL, FOOD, or OTHER.
